# Blog

## Build

Build website on `/site`.

```shell
haunt build
```

## Test

Open web server on `http://localhost:8080`.

```shell
haunt serve
```
