;;; Copyright (C) 2019-2024 Pierre-Antoine Rouby <contact@parouby.fr>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;; Some piece of code come from David Thompson blog (GPLv3+).
;;   https://git.dthompson.us/blog.git

(use-modules (haunt asset)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt html)
             (haunt page)
             (haunt post)
             (haunt reader)
             (haunt reader commonmark)
             ;; (haunt reader skribe)
             (haunt site)
             (haunt utils)
             (commonmark)
             (sxml match)
             (sxml transform)
             (srfi srfi-1)
             (srfi srfi-19)
             (ice-9 rdelim)
             (ice-9 regex)
             (ice-9 match)
             (tools cv)
             (tools highlight)
             (themes main))

;;;
;;; Static page
;;;

(define (static-page title file-name body)
  (lambda (site posts)
    (make-page file-name
               (with-layout main-theme site title body)
               sxml->html)))

;;;
;;; CV
;;;

(define cv-page
  (static-page
   "Curriculum vitae"
   "cv.html"
   `(,(h1 "Curriculum Vitae")
     ,(cv-json->sxml (string-append (dirname (current-filename))
                                    "/cv.json")))))

;;;
;;; LCO page
;;;

(define lco-page
  (static-page
   "Dissonance cognitive"
   "dissonance_cognitive.html"
   '((h1 "Dissonance cognitive")
     (ul
      (li (p (a (@ (href "/pub/lco/")) "Télécharger l'affiche et les sources.")))
      (li (p (a (@ (href "https://fr.wikipedia.org/wiki/Dissonance_cognitive"))
                "Wikipédia")))
      (li (p (a (@ (href "https://www.youtube.com/watch?v=Hf-KkI2U8b8"))
                "Vidéo de \"la Tronche en Biais\" sur le sujet.")))
      (li (p (a (@ (href "https://www.youtube.com/channel/UCGeFgMJfWclTWuPw8Ok5FUQ"))
                "Chaine YouTube de Horizon Gull.")))))))

;;;
;;; Projects
;;;

(define (project-item label url date description)
  `((h2 (a (@ (class "project-a")
             (href ,url))
          ,label))
    (p (small (em "(",date ")  ")) ,description)))

(define projects-page
  (static-page
   "Projects"
   "projects.html"
   `(,(h1 "Projects")
     ,(project-item "Pamhyr2" "/projects/pamhyr2.html"
                    "2023-2024"
                    "Pamhyr2 : Interface de calcul pour les modèles de rivière en hydraulique 1D.")
     ,(project-item "Librepoll" "/projects/librepoll.html"
                    "2018-2019"  "Self hosted opinion poll service.")
     ,(project-item "Efetch" "/projects/efetch.html"
                    "2018-2019" "Display system configuration on GNU Emacs buffer.")
     ,(project-item "AmnesiaRemove" "/projects/AmnesiaRemove.html"
                    "2018-2019" "The UNIX rm command with overwrite options.")
     ,(project-item "Guile-mastodon" "/projects/guile-mastodon.html"
                    "2018" "Guile module for Mastodon API."))))

(define %framagit
  `(a (@ (href "https://framagit.org/")
         (target "_blank"))
      "Framagit"))

(define %gitlab-irstea
  `(a (@ (href "https://gitlab.irstea.fr/")
         (target "_blank"))
      "Gitlab Irstea"))

(define %autotools
  `(code
    "./bootstrap" (br)
    "./configure" (br)
    "make" (br)
    "make install"))

(define %guix-local-install
  `(code "guix package -f "
         (span (@ (class "syntax-keyword"))
               "guix.scm")))

(define* (project-page #:key name file description
                       badges repo license)
  (define body
    `(,(h1 name)
      ,@(if badges
            (map (λ (i)
                   `(img (@ (class "badges")
                            (src ,i))))
                 badges)
            '())
      ,description
      (h2 "Source Code")
      (p ,name " is hosted on "
         ,@(match (caddr (string-split repo #\/))
             ("framagit.org" `("Framasoft Gitlab (" ,%framagit ")"))
             ("gitlab.irstea.fr" `("INRAE gitlab (" ,%gitlab-irstea ")")))
         ". See the repo "
         (a (@ (href ,repo)
               (target "_blank"))
            "here") ".")
      (code ,(string-append "git clone " repo))
      (h2 "License")
      (p ,name " is Free Software under " (strong ,license))))
  (static-page name (string-append "projects/" file) body))

(define pamhyr2-page
  (project-page
   #:name "Pamhyr2"
   #:file "pamhyr2.html"
   #:description
   `((p "Pamhyr2: A Graphical User Interface for 1D Hydro-Sedimentary Modelling
of Rivers ("
        (a (@ (href
               "https://link.springer.com/chapter/10.1007/978-981-97-4076-5_33")
              (target "_blank"))
           "publication")
        ", "
        (a (@ (href "https://gitlab.irstea.fr/theophile.terraz/pamhyr/-/blob/master/doc/paper/2023/simhydro/paper.pdf"))
           "preprint")
        ").")
     (img (@ (class "centred-img")
             (src "/images/pamhyr2.png")
             (alt "Pamhyr2 screenshot.")
             (width "80%")))
     (p "In numerical river simulation, one-dimensional models are commonly
used to study water level and discharge for large domains or long time
series. These models are less time-consuming than two- and
three-dimensional numerical models, and require fewer input parameters
and allow ensemble runs. To build a one-dimensional hydraulic model, a
pre- and post-processing tool is needed for creating reach geometry,
specifying initial and boundary conditions, friction coefficients and
other numerical parameters. Such a tool needs to ensure the
consistency of the model and provide a user-friendly graphical user
interface. In this article, we present Pamhyr2, the fully rebuilt
version of the PAMHYR modelling platform. It is developed using
Python, PyQt and Matplotlib. Pamhyr2 is free and open-source,
multilingual, cross-platform (Linux, Windows) and is generic enough to
accept various one-dimensional solvers. Pamhyr2 includes and enhances
all the features from the previous version: multiple-reach modelling,
geometry definition from cross-section data, initial and boundary
conditions, friction coefficients, hydraulic structures, lateral
inflow, punctual intake and the results visualization window. In
addition, this version includes new features such as pollutants
modelling, bed-load and bed evolution. We describe several windows:
the creation of sedimentary layers in the river bed, the sediment
characteristics for each layer, the sediment, and pollutant boundary
conditions and the lateral inputs. These functionalities are
illustrated with simple examples. We finally show the visualization
windows for bed evolution."))
   #:badges
   '("https://gitlab.irstea.fr/theophile.terraz/pamhyr/badges/master/pipeline.svg"
     "https://gitlab.irstea.fr/theophile.terraz/pamhyr/-/badges/release.svg")
   #:repo "https://gitlab.irstea.fr/theophile.terraz/pamhyr"
   #:license "GNU GPL3+"))

(define librepoll-page
  (project-page
   #:name "LibrePoll"
   #:file "librepoll.html"
   #:description
   `((p "Librepoll is a free opinion poll service. It's designed to be
self hosted service.")
     (img (@ (class "centred-img")
             (src "/images/librepoll.png")
             (alt "LibrePoll screenshot."))))
   #:badges
   '("https://img.shields.io/badge/License-agpl3+-blue.svg?logo=gnu&logoColor=white"
     "https://img.shields.io/badge/Release-0.0.0-blue.svg?logo=gitlab")
   #:repo "https://framagit.org/prouby/librepoll"
   #:license "GNU AGPL3+"))

(define efetch-page
  (project-page
   #:name "Efetch"
   #:file "efetch.html"
   #:description
   `((p "Emacs extention to see your system configuration.")
     (img (@ (class "centred-img")
             (src "/images/efetch.png")
             (alt "Efetch screenshot."))))
   #:badges
   '("https://img.shields.io/badge/Release-0.0.5-blue.svg?logo=gitlab"
     "https://img.shields.io/badge/Guix-local_package-yellow.svg?logo=gnu&logoColor=white"
     "https://img.shields.io/badge/License-gpl3+-blue.svg?logo=gnu&logoColor=white"
     "https://framagit.org/prouby/emacs-fetch/badges/master/pipeline.svg")
   #:repo "https://framagit.org/prouby/emacs-fetch"
   #:license "GNU GPL3+"))

(define amnesia-page
  (project-page
   #:name "AmnesiaRemove"
   #:file "AmnesiaRemove.html"
   #:description
   `((p "The UNIX rm command with overwrite options.")
     (h2 "Install")
     (p "With autotools.")
     ,%autotools
     (p "With Guix.")
     ,%guix-local-install)
   #:badges
   '("https://img.shields.io/badge/Guix-local_package-yellow.svg?logo=gnu&logoColor=white"
     "https://img.shields.io/badge/License-gpl3+-blue.svg?logo=gnu&logoColor=white"
     "https://framagit.org/prouby/AmnesiaRemove/badges/master/pipeline.svg")
   #:repo "https://framagit.org/prouby/AmnesiaRemove.git"
   #:license "GNU GPL3+"))

(define guile-mastodon-page
  (project-page
   #:name "Guile-Mastodon"
   #:file "guile-mastodon.html"
   #:description
   `((p "GNU Guile module implementing Mastodon REST API protocol.")
     (h2 "Install")
     (h3 "Autotools")
     (code
      "./bootstrap" (br)
      "./configure" (br)
      "make" (br)
      "make install")
     (h3 "Guix")
     (p "Use local packages")
     (code "guix package -f " (span (@ (class "syntax-keyword"))
                                    "guix.scm"))
     (p "Release 0.0.1 is available on "
        (a (@ (href "https://www.gnu.org/software/guix/"))
           "GuixSD"))
     (code "guix package -i " (span (@ (class "syntax-keyword"))
                                    "guile-mastodon"))
     (h2 "Example")
     ,(highlight-scheme
       "(use-modules (mastodon)
             (mastodon types))

(let ((my-inst (make-mastodon \"Framapiaf\"
                              \"https://framapiaf.org\"
                              \"<acces_token>\")))
  (new-status my-inst
              #:status \"Hello from guile-mastodon !\"))"))
   #:badges
   '("https://framagit.org/prouby/guile-mastodon/badges/master/pipeline.svg"
     "https://img.shields.io/badge/License-gpl3+-blue.svg?logo=gnu&logoColor=white"
     "https://img.shields.io/badge/Mastodon-API_REST-blue.svg?logo=mastodon&logoColor=white")
   #:repo "https://framagit.org/prouby/guile-mastodon"
   #:license "GNU GPL3+"))

;;;
;;; Site
;;;

(site #:title "Pierre-Antoine Rouby"
      #:domain "parouby.fr"
      #:default-metadata
      '((author . "Pierre-Antoine Rouby")
        (email  . "contact@parouby.fr"))
      #:readers (list #|skribe-reader|# commonmark-reader*)
      #:builders (list (blog #:theme main-theme)
                       (atom-feed #:blog-prefix "/")
                       cv-page
                       projects-page
                       pamhyr2-page
                       librepoll-page
                       efetch-page
                       amnesia-page
                       guile-mastodon-page

                       ;; LCO (Univercity work)
                       lco-page

                       (static-directory "css")
                       (static-directory "pub")
                       (static-directory "images")))

;;; haunt.scm ends here.
