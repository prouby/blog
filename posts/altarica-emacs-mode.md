title: AltaRica Emacs major mode
date: 2021-01-20 11:00
tags: altrica, emacs, haunt, french
summary: Emacs major mode for AltaRica file
---

Emacs major mode for [AltaRica](https://altarica.labri.fr/wp/).

![Screen](/images/altarica-mode.png)

[Download the mode](https://parouby.fr/pub/emacs/altarica-mode.el).
