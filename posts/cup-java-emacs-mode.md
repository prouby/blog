title: Cup java Emacs major mode
date: 2020-03-23 14:00
tags: java, emacs, cup, haunt, french
summary: Emacs major mode for Cup java
---

Emacs major mode for [Cup](https://www.cs.princeton.edu/~appel/modern/java/CUP/manual.html).

![Screen](/images/cup-java-mode.png)

[Download the mode](https://parouby.fr/pub/emacs/cup-java-mode.el).
