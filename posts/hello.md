title: Nouveau site web !
date: 2019-05-30 11:00
tags: webdev, haunt, french
summary: Hello !
---

La nouvelle version de mon site web est en ligne !

Touts en Scheme ([GNU Guile](https://gnu.org/s/guix)), il est généré
de façon statique avec le logiciel
[Haunt](https://dthompson.us/projects/haunt.html).

Le code source est disponible [ici](https://framagit.org/prouby/blog).
