;;; Copyright (C) 2021 by Pierre-Antoine Rouby <contact@parouby.fr>

(defun make-transition (from with to)
  (list from with to))

(defun make-buchi-automata (states transitions initials finals)
  (list states transitions initials finals))

(defun ba-states (A) (car A))
(defun ba-transitions (A) (cadr A))
(defun ba-initials (A) (caddr A))
(defun ba-finals (A) (cadddr A))

(defun ba-purify (A)
  "Remove transition simplification in A automata and return a
new automata:

((q1, 'a,b', q2)) -> ((q1, 'a' , q2) (q1, 'b', q2))"
  (let ((S (ba-states A))
        (T (ba-transitions A))
        (I (ba-initials A))
        (F (ba-finals A)))
    (make-buchi-automata
     S
     (reduce 'append
             (mapcar
              (lambda (e)
                (let ((from  (car   e))
                      (label (cadr  e))
                      (to    (caddr e)))
                  (mapcar
                   (lambda (l)
                     (list from l to))
                   (split-string label ","))))
              T))
     I
     F)))

;;;
;;; Usefull function
;;;

(defun ba-is-deterministic? (A)
  "Complexity is O(m²) with: m = |Transitions(A)|

Works only if |I| == 1 ..."
  (let ((S (ba-states A))
        (T (ba-transitions A))
        (I (ba-initials A))
        (F (ba-finals A)))
    (not (reduce (lambda (p1 p2)
                 (or p1 p2))
               (reduce 'append
                       (mapcar (lambda (t1)
                                 (let ((from1  (car   t1))
                                       (label1 (cadr  t1))
                                       (to1    (caddr t1)))
                                   (mapcar (lambda (t2)
                                             (let ((from2  (car   t2))
                                                   (label2 (cadr  t2))
                                                   (to2    (caddr t2)))
                                               (and (equal from1 from2)
                                                  (equal label1 label2)
                                                  (not (equal to1 to2)))))
                                           T)))
                               T))))))

;;;
;;; Latex
;;;

(defun ba->latex-formula (A &optional no-transition)
  (let ((A (ba-purify A)))
    (let ((S (ba-states A))
          (T (ba-transitions A))
          (I (ba-initials A))
          (F (ba-finals A)))
      (format
       "\\begin{align*}
  S = &\\{ %s \\} \\\\
  T = &\\{ %s \\} \\\\
  I = &\\{ %s \\} \\\\
  F = &\\{ %s \\}
\\end{align*}"
       (string-join S ", ")
       (string-join
        (if no-transition
            (let ((x (car T))
                  (y (car (last T))))
              (list (format "(%s \\xrightarrow{%s} %s), \\dots, (%s \\xrightarrow{%s} %s)"
                            (car x) (cadr x) (caddr x)
                            (car y) (cadr y) (caddr y))))
          (mapcar (lambda (x)
                    (format
                     "(%s \\xrightarrow{%s} %s)"
                     (car x) (cadr x) (caddr x)))
                  T))
        ",")
       (string-join I ",")
       (string-join F ",")))))

;;;
;;; Dot
;;;

(defun ba->dot (A)
  (let ((S (ba-states A))
        (T (ba-transitions A))
        (I (ba-initials A))
        (F (ba-finals A)))
    (format
     "digraph {\n\trankdir=LR;\n%s\n%s\n%s\n%s\n}"
     (string-join
      (mapcar (lambda (x)
                (format "\ti%s[shape=point,label=\"\"];" x))
              I)
      "\n")
     (string-join
      (mapcar (lambda (x)
                (format "\t%s[shape=%s];"
                        x (if (member x F)
                              "doublecircle"
                            "circle")))
              S)
      "\n")
     (string-join
      (remove ""
              (mapcar (lambda (x)
                        (format "%s"
                                (if (member x I)
                                    (format
                                     "\ti%s -> %s;"
                                     x x)
                                  "")))
                      S))
      "\n")
     (string-join
      (mapcar (lambda (x)
                (format "\t%s -> %s[label=\"%s\"];"
                        (car x) (caddr x)
                        (cadr x)))
              T)
      "\n"))))

(defun ba->png (A name)
  (let ((buffer (find-file (format "%s.dot" name))))
    (with-current-buffer buffer
      (erase-buffer)
      (insert (ba->dot A))
      (save-buffer)
      (shell-command-to-string (format "dot -Tpng %s.dot -o %s.png" name name)))
    (kill-buffer buffer)
    (format "%s.png" name)))

;;;
;;; Automate to C
;;;

(defun ba->c (A)
  (let ((A (ba-purify A)))
    (let ((S (ba-states A))
          (T (ba-transitions A))
          (I (ba-initials A))
          (F (ba-finals A)))
      (format
       "/* This file is auto generate file... */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define OK 0
#define FAILED -1

/* State prototype */
%s

/* State */
%s

/* Main */
int
main (int argc, char **argv)
{
  char * w = argv[1];
  if (w == NULL)
    return FAILED;

  bool res = (%s);

  if (res)
    {
      printf(\"[OK] '%%s' is a valid word! :)\\n\", w);
    }
  else
    {
      printf(\"[FAILED] '%%s' is not a valid word! :(\\n\", w);
    }

  return res ? OK : FAILED;
}
"
       (string-join
        (mapcar (lambda (x)
                  (format "int state_%s (char * w);" x))
                S)
        "\n")
       (string-join
        (mapcar (lambda (x)
                  (format "
int
state_%s (char * w)
{
  if (w[0] == '\\0')
    {
      bool is_ok = %s;
      printf(\"[END] word finish on state '%s' with status %%s\\n\",
             is_ok ? \"OK\": \"FAILED\");
      return is_ok;
    }

  bool res = (%s);

  return (res);
}
"
                          x
                          (if (member x F)
                              "true"
                            "false")
                          x
                          (string-join
                           (mapcar (lambda (e)
                                     (let ((from  (car   e))
                                           (label (cadr  e))
                                           (to    (caddr e)))
                                       (if (equal from x)
                                           (progn
                                             (format
                                              "((w[0] == '%s') && (state_%s(w+1)))"
                                              label to))
                                         "(false)")))
                                   T)
                           "||\n    ")))
                S)
        "\n")
       (string-join
        (mapcar (lambda (i)
                  (format "((state_%s(w)))"
                          i))
                I)
        "||\n  ")))))

(defun ba->finite-word-automata (A name)
  (let ((c-code (ba->c A)))
    (mkdir "/tmp/emacs-automata/" t)
    (if c-code
        (progn
          (let ((buffer (find-file (format "/tmp/emacs-automata/%s.c" name))))
            (with-current-buffer buffer
              (erase-buffer)
              (insert c-code)
              (save-buffer)
              (shell-command-to-string
               (format "gcc -O3 -o /tmp/emacs-automata/%s /tmp/emacs-automata/%s.c"
                       name name)))
            (kill-buffer buffer)
            (format "/tmp/emacs-automata/%s" name)))
      nil)))

(defun ba-try-on-finite-word (A word &optional prog)
  (let ((exec (if (not prog)
                  (ba->finite-word-automata A "automata-tmp")
                prog)))
    (= (shell-command
        (format "%s %s" exec word))
       0)))

;;;
;;; Test
;;;

(defvar A nil)
(setq A (make-buchi-automata
         '("q1" "q2")
         '(("q1" "a" "q1")
           ("q1" "b" "q2")
           ("q2" "a" "q2"))
         '("q1")
         '("q2")))
(assert (ba-is-deterministic? A))

(defvar nd-A nil)
(setq nd-A (make-buchi-automata
            '("q1" "q2")
            '(("q1" "a" "q1")
              ("q1" "b" "q1")
              ("q1" "b" "q2")
              ("q2" "a" "q2"))
            '("q1")
            '("q2")))
(assert (not (ba-is-deterministic? nd-A)))
