;;;Copyright (C) 2021 by Pierre-Antoine Rouby <contact@parouby.fr>

(defun edge-reverse (e)
  (let ((s1 (car e))
        (s2 (cdr e)))
    (cons s2 s1)))

(defun mk-graph (nodes edges &optional sup)
  "G = (V,E)"
  (list nodes edges sup))

(defun graph-nodes (g) (car g))
(defun graph-edges (g) (cadr g))
(defun graph-n (g) (length (graph-nodes g)))
(defun graph-m (g) (length (graph-edges g)))

(defun graph-colors (g)
  (let ((sup (caddr g)))
    (if sup
        (assoc "color" sup)
      '())))

(defun graph-add-node (g node)
  (let ((V (graph-nodes g))
        (E (graph-edges g)))
    (list (cons node V)
          E)))

(defun graph-add-edge (g a b)
  (let* ((V (graph-nodes g))
         (E (graph-edges g))
         (as-a (not (null (member a V))))
         (as-b (not (null (member b V)))))
    (let ((V (if as-a V (cons a V))))
      (let ((V (if as-b V (cons b V))))
        (list V
              (cons (cons a b) E))))))

(defun graph-node-neighbour (g s)
  (let ((V (graph-nodes g))
        (E (graph-edges g)))
    (map-filter (lambda (x y) (equal x s))
                (append E (mapcar 'edge-reverse E)))))

(defun graph-node-post (g s)
  (let ((V (graph-nodes g))
        (E (graph-edges g)))
    (mapcar 'cdr (map-filter (lambda (x y) (equal x s))
                             E))))

(defun graph-node-pre (g s)
  (let ((V (graph-nodes g))
        (E (graph-edges g)))
    (mapcar 'car (map-filter (lambda (x y) (equal y s))
                             E))))


(defun graph-delta (g)
  "\delta(G) is the maximun degrees of G graph."
  (let ((V (graph-nodes g))
        (E (graph-edges g)))
    (apply 'max
           (mapcar (lambda (s)
                     (length (graph-node-neighbour g s)))
                   V))))

(defun graph-dot-string (g di)
  (let ((V (graph-nodes g))
        (E (graph-edges g))
        (C (graph-colors g))
        (arrow (if di "->" "--")))
    (let ((sV (mapcar (lambda (x)
                        (format "%s%s;" x
                                (let ((color (assoc x C)))
                                  (if color
                                      (format " [color=\"%s\"]" (cdr color))
                                    ""))))
                      V))
          (sE (mapcar (lambda (x)
                        (format "%s %s %s;" (car x) arrow (cdr x)))
                      E)))
      (format "digraph { \n\tnode [shape=circle];\n\trankdir=LR;\n\t%s\n\t%s\n}"
              (string-join sV "\n\t")
              (string-join sE "\n\t")))))

(defun graph-to-png (g name)
  (let ((buffer (find-file (format "%s.dot" name))))
    (with-current-buffer buffer
      (erase-buffer)
      (insert (graph-dot-string g nil))
      (save-buffer)
      (shell-command-to-string (format "dot -Tpng %s.dot -o %s.png" name name)))
    (format "%s.png" name)))

(defun digraph-to-png (g name)
  (let ((buffer (find-file (format "%s.dot" name))))
    (with-current-buffer buffer
      (erase-buffer)
      (insert (graph-dot-string g t))
      (save-buffer)
      (shell-command-to-string (format "dot -Tpng %s.dot -o %s.png" name name)))
    (format "%s.png" name)))


;;; Ensemble

(defun ens-priv (S1 S2)
  "S1 \ S2: O(n)"
  (cond
   ((equal S2 nil) S1)
   (t (let* ((q (car S2))
             (qS1 (member q S1)))
        (ens-priv (if qS1
                        (remove q S1)
                      S1)
                    (cdr S2))))))

(defun ens-add (S q)
  "S U {q}: O(1)"
  (cons q S))

(defun ens-in (S q)
  "q in S: O(n)"
  (member q S))

(defun uniq-aux (l acc)
  (if (equal l nil)
      acc
    (if (ens-in acc (car l))
        (uniq-aux (cdr l) acc)
      (uniq-aux (cdr l) (cons (car l) acc)))))

(defun uniq (l)
  "O(n)"
  (uniq-aux l '()))

(defun ens-union (S1 S2)
  "O(n)"
  (uniq (append S1 S2)))

(defun ens-inter-aux (S1 S2 acc)
  (if (equal S1 nil)
      acc
    (ens-inter-aux
     (cdr S1)
     S2
     (if (and (ens-in S2 (car S1))
              (not (ens-in acc (car S1))))
         (cons (car S1) acc)
       acc))))

(defun ens-inter (S1 S2)
  "O(n²)"
  (ens-inter-aux S1 S2 '()))


;;; Sat

(defun sat-EG (SPsi G)
  "Sat(EG Psi)"
  (let* ((S (graph-nodes G))
         (V (ens-priv S SPsi))
         (T SPsi)
         (cq (make-hash-table :test 'equal))
         (cql (mapcar (lambda (x)
                        (let ((n (length (graph-node-post G x))))
                          (puthash x n cq)
                          n))
                      SPsi)))
    (while V
      (let* ((qp (car V))
             (pre (graph-node-pre G qp)))
        (setq V (ens-priv V (list qp)))
        (while pre
          (let ((q (car pre)))
            (setq pre (ens-priv pre (list q)))
            (when (ens-in T q)
              (puthash q (- (gethash q cq) 1) cq)
              (when (= (gethash q cq) 0)
                (setq T (ens-priv T (list q)))
                (setq V (ens-add V q))))))))
    T))

(defun sat-EU (SPsi1 SPsi2 G)
  "Sat(E Psi_1 U Psi_2)"
  (let* ((S (graph-nodes G))
         (V SPsi2)
         (T SPsi2))
    (while V
      (let* ((qp (car V))
             (pre (graph-node-pre G qp)))
        (setq V (ens-priv V (list qp)))
        (while pre
          (let ((q (car pre)))
            (setq pre (ens-priv pre (list q)))
            (when (ens-in (ens-priv SPsi1 T) q)
              (setq V (ens-add V q))
              (setq T (ens-add T q)))))))
    T))

(defun sat-EF (SPsi G)
  "Sat(EF PSi) === sat-(E true U PSi)"
  (sat-EU (graph-nodes G)               ;sat-(true) === S
          SPsi G))

(defun sat-EX (SPsi G)
  (throw 'sat "Not implemented yet"))

(defun sat-and (SPsi1 SPsi2 G)
  "Sat(PSi1 /\\ PSi2)"
  (ens-inter SPsi1 SPsi2))

(defun sat-neg (SPsi G)
  "Sat(¬PSi)"
  (ens-priv (graph-nodes G)
            SPsi))

;;; Test

(setq G-test
  (mk-graph '("a" "b" "c")
            '(("a" . "b")
              ("b" . "c"))))

(setq G-test-2
  (mk-graph '("a" "b" "c" "d" "e")
            '(("a" . "b")
              ("b" . "c")
              ("d" . "c")
              ("d" . "e")
              ("e" . "d"))))

(defun res-sort (e)
  (sort e
        (lambda (x y)
          (not (string-greaterp x y)))))

(defun test (test e1 e2)
  (let ((se1 (res-sort e1))
        (se2 (res-sort e2)))
    (if (equal se1 se2)
        (message "[TEST] %s:\t\tOk --> %s === %s" test se1 se2)
      (message "[TEST] %s:\tFailed --> %s === %s" test se1
      se2))))

(test "neg-ab"
      '("c")
      (sat-neg '("a" "b") G-test))

(test "neg-2"
      '("c" "d" "e")
      (sat-neg '("a" "b") G-test-2))

(test "and"
      '("b")
      (sat-and '("a" "b") '("c" "b") G-test))

(test "EU"
      '("a" "b" "c")
      (sat-EU '("a" "b") '("c") G-test))

(test "EU t2"
      '("c")
      (sat-EU '("a") '("c") G-test))

(test "EG"
      '("a" "b" "c")
      (sat-EG '("a" "b" "c") G-test))

(test "EG t2"
      '("a" "b" "c")
      (sat-EG '("a" "b" "c" "e") G-test-2))

(test "EG t3"
      '("d" "e")
      (sat-EG '("a" "b" "d" "e") G-test-2))

(test "EF"
      '("d" "e")
      (sat-EF '("d") G-test-2))
