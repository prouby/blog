;;; AltaRica -- Emacs AltaRica mode              -*- coding: utf-8; -*-
;;;
;;; Copyright (C) 2021 by Pierre-Antoine Rouby <contact@parouby.fr>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <https://www.gnu.org/licenses/>.

(defgroup altarica-mode nil
  "Support for Altarica."
  :link '(url-link "https://altarica.labri.fr/wp/")
  :group 'languages)

(defcustom altarica-indent-offset 2
  "Indent altarica by this number of spaces."
  :type 'integer
  :group 'altarica-mode
  :safe #'integerp)

(defcustom altarica-version "0.0.1"
  "AltaRica version."
  :type 'string
  :group 'altarica-mode)

(defcustom altarica-arc-bin "arc"
  "AltaRica arc bin name."
  :type 'string
  :group 'altarica-mode)

(defcustom altarica-file-suffix "\\.alt$"
  "AltaRica model file suffix."
  :type 'string
  :group 'altarica-mode)

(defcustom altarica-el-file-suffix "\\.elalt$"
  "AltaRica emacs-lisp model file suffix."
  :type 'string
  :group 'altarica-mode)

(defcustom altarica-spe-file-suffix "\\.spe$"
  "AltaRica specification file suffix."
  :type 'string
  :group 'altarica-mode)

(defcustom altaricat-file-header ""
  "Altarica file header."
  :type 'string
  :group 'altarica-mode)

;;; Use '.alt' as AltaRica file suffix
(add-to-list 'auto-mode-alist
             `(,altarica-file-suffix . altarica-mode))

(add-to-list 'auto-mode-alist
             `(,altarica-el-file-suffix . altarica-el-mode))

(add-to-list 'auto-mode-alist
             `(,altarica-spe-file-suffix . altarica-spe-mode))


(defun arc-eval-elisp-string (str)
  "Eval STR as sexp in string format. Return t on success and nil
on error."
  (with-temp-buffer
    (let ((to-eval (format "(setq arc-last-eval %s)" str)))
      (insert to-eval)
      (condition-case nil
          (progn (eval-buffer) arc-last-eval)
        (error (message (format "arc-eval-elisp-string: Failed to eval %s"
                                str))
               nil)))))

(defun arc-pre-var-replace (str var values)
  "AltaRica preprocessor fuction to replace VAR with VALUES in
STR."
  (string-join
   (mapcar
    (lambda (x)
      (format "%s%s"
              arc-pre-current-indent
              (string-join (split-string str var)
                           (format "%d" x))))
    values)))

(defun arc-preprocessor (buffer)
  (with-current-buffer buffer
    (fundamental-mode)
    (goto-char (point-min))
    (while (not (= (point) (point-max)))
      (let ((code-start (search-forward "<?elisp" nil t))
            (code-end   (search-forward "?>" nil t)))
        (if code-start
            (progn
              (goto-char code-start)
              (beginning-of-line)
              ;; Compute indent
              (setq arc-pre-current-indent "")
              (let ((x (- code-start (point) 7)))
                (while (> x 0)
                  (setq arc-pre-current-indent
                        (format " %s" arc-pre-current-indent))
                  (setq x (- x 1)))
                ;; Substitute
                (let* ((code     (buffer-substring code-start (- code-end 2)))
                       (evaluate (arc-eval-elisp-string code))
                       (old (delete-and-extract-region (- code-start 7)
                                                       code-end))
                       (commented-old (string-join
                                       (mapcar
                                        (lambda (str)
                                          (let ((str (string-trim str)))
                                            (if (equal str "")
                                                ""
                                                (format "%s  // %s"
                                                        arc-pre-current-indent
                                                        str))))
                                        (split-string
                                         (substring old 8 (- (length old) 2))
                                         "\n" t " "))
                                       "\n")))
                  (if (stringp evaluate)
                      (insert evaluate)
                    (insert (format "// -> %S\n" evaluate))))))
          (progn
            (goto-char (point-max))
            (altarica-mode)))))))

(defun arc-last (lst)
  (nth (- (length lst) 1) lst))

;;;
;;; INTERACTIVE
;;;

(defun altarica-new-model (name)
  "Create new AltaRica file."
  (interactive "sAutomate name: ")
  (insert (format "%s\nnode %s\n  %s\nedon"
                  altaricat-file-header
                  name
                  "/* ... */")))

(defun altarica-new-elisp-block ()
  "Create new AltaRica file."
  (interactive)
  (insert "<?elisp    ?>"))

(defun altarica-new-file (name)
  "Create new AltaRica file."
  (interactive "sAutomate name: ")
  (let ((filename (format "%s.alt" name))
        (automatename (capitalize name)))
    (let ((buffer (find-file filename)))
      (with-current-buffer buffer
        (erase-buffer)
        (altarica-new-model automatename)))))

(defun altarica-load-on-altarica-studio ()
  "Load current buffer on altarica-studio."
  (interactive)
  (let ((name (buffer-file-name)))
    (message "Running AltaRica studio!")
    (start-process "AltaRica studio" "*AltaRica studio*" "altarica-studio" name)))

(defun altarica-preprocessor-current-buffer ()
  "Create new AltaRica file."
  (interactive)
  (let ((original-name (arc-last (split-string (buffer-file-name) "/")))
        (file-str (with-current-buffer (current-buffer)
                    (buffer-substring-no-properties (point-min)
                                                    (point-max)))))
    (let* ((original-md5 (md5 file-str))
           (filename (format "%s-%s.alt"
                             (car (split-string original-name
                                                "\\.elalt" t))
                             (substring original-md5 0 5)))
           (buffer   (find-file filename)))
      (with-current-buffer buffer
        (erase-buffer)
        (insert (format "/* %s --- AltaRica file generate with Emacs.

   Emacs verion:   %s
   Altariace-mode: %s

   Creation date:  %s
   Source file:    %s (%s)

   -*- mode: %s; -*-
 */

%s

/* %s ends here. */"
                        filename
                        (emacs-version) altarica-version
                        (current-time-string) original-name original-md5
                        "altarica"
                        file-str
                        filename))
        (arc-preprocessor buffer)
        (save-buffer)
        (read-only-mode)
        ;; Running altarica
        (altarica-load-on-altarica-studio)))))



;;;
;;; KEYMAP
;;;

(defvar altarica-mode-map (make-sparse-keymap)
  "Keymap for `altarica-mode'")

(define-key altarica-mode-map (kbd "C-c C-f") 'altarica-new-file)
(define-key altarica-mode-map (kbd "C-c C-a") 'altarica-new-model)
(define-key altarica-mode-map (kbd "C-c C-c") 'altarica-load-on-altarica-studio)

(defvar altarica-el-mode-map (make-sparse-keymap)
  "Keymap for `altarica-el-mode'")

(define-key altarica-el-mode-map (kbd "C-c C-f") 'altarica-new-file)
(define-key altarica-el-mode-map (kbd "C-c C-a") 'altarica-new-model)
(define-key altarica-el-mode-map (kbd "C-c C-e") 'altarica-new-elisp-block)
(define-key altarica-el-mode-map (kbd "C-c C-c") 'altarica-preprocessor-current-buffer)


;;;
;;; HIGHLIGHTS
;;;

(setq altarica-keywords
      '("and" "assert" "bool" "case" "const"
        "domain" "edon" "else" "event" "extern"
        "false" "flow" "if" "imply" "init"
        "integer" "max" "min" "mod" "node"
        "not" "or" "param" "param_set" "sig"
        "sort" "state" "struct" "sub" "sync"
        "tcurts" "then" "trans" "true"))

(setq altarica-spe-keywords
      '("with" "do" "done"))

(setq altarica-spe-builtin
      '("any_c" "valid_state_assignments" "empty_s" "any_s" "initial"
        "any_t" "any_trans" "epsilon" "not_deterministic"
        "self" "self_epsilon" "valid_state_changes"
        "assert" "and" "or" "coreach" "label" "attribute" "loop"
        "not" "pick" "proj_f" "proj_s" "reach" "rsrc" "rtgt"
        "src" "tgt" "trace" "unav"
        "display" "ctl2mu" "dot" "dot-trace" "events" "nodes"
        "nrtest" "project" "quot" "remove" "show" "test"
        "validate" "wts"))

(setq altarica-multiline-comment-regex
      "\\/\\*+\\([^\\*]\\|\\*[^\\/]\\|\\*\\\\\\/\\|\n\\)*\\*+\\/")

(setq altarica-line-comment-regex
      "\\(\\/\\/\\)\\(.*\\|\"\\)*$")

(setq altarica-highlights
      `((,altarica-multiline-comment-regex . font-lock-comment-face)
        (,altarica-line-comment-regex . font-lock-comment-face)
        (,(concat "\\(^\\|[^a-zA-Z]\\)\\("
           (string-join altarica-keywords
                        "\\|")
           "\\)\\([^a-zA-Z]\\|$\\)")
         . (2 font-lock-keyword-face))
        ("node[ \t]+\\([a-zA-Z0-9_]+\\)" . (1 font-lock-variable-name-face))
        ("\\(const\\|domain\\)[ \t]+\\([a-zA-Z0-9_]+\\)"
         . (2 font-lock-variable-name-face))
        (,(string-join '("|-" "->" "!=" "<=" ">=" ":=" "=>" "~")
                       "\\|")
         . font-lock-constant-face)
        ("[?<>|&/:*]+" . font-lock-constant-face)))

(setq altarica-el-highlights
      (append '((";;.*$" . font-lock-comment-face)
                ("<\\?elisp\\|\\?>" . font-lock-function-name-face)
                ;; TODO: Add elisp keyword
                )
              altarica-highlights))


(setq altarica-spe-highlights
      `((,altarica-multiline-comment-regex . font-lock-comment-face)
        (,altarica-line-comment-regex      . font-lock-comment-face)
        ("'\\([^\\']\\|\\\\'\\)+'"  . font-lock-string-face)
        ("^[ \t]+\\(.*\\)[ \t]+:="  . (1 font-lock-variable-name-face))
        (,(concat "\\(^\\|[^a-zA-Z]\\)\\("
           (string-join altarica-spe-keywords
                        "\\|")
           "\\)\\([^a-zA-Z]\\|$\\)")
         . (2 font-lock-keyword-face))
        (,(string-join '("|-" "->" "!=" "<=" ">=" ":=" "=>" "~")
                       "\\|")
         . font-lock-constant-face)
        ("[?{}<>=|&/:*]+" . font-lock-constant-face)
        (,(concat "\\("
                  (string-join altarica-spe-builtin "\\|")
                  "\\)[^a-zA-Z0-9]+")
         . (1 font-lock-builtin-face))))


;;;
;;; MODE
;;;

(defun altaricat-mode-commun-locals ()
  (set (make-local-variable 'font-lock-multiline) t)

  (setq-local comment-start "/* ")
  (setq-local comment-start-skip "/\\*+[ \t]*")
  (setq-local comment-end " */")
  (setq-local comment-end-skip "[ \t]*\\*+/")

  ;; Indentation
  (setq-local indent-tabs-mode nil)
  (setq-local tab-width altarica-indent-offset)
  (setq-local indent-line-function 'indent-to-left-margin)

  ;; Keymap
  (use-local-map altarica-mode-map))

;;;###autoload
(define-derived-mode altarica-mode fundamental-mode "altarica"
  "Major mode for editing AltaRica description language."
  (altaricat-mode-commun-locals)
  (setq-local font-lock-defaults '(altarica-highlights)))

;;;###autoload
(define-derived-mode altarica-el-mode altarica-mode "altarica-el"
  "Major mode for editing AltaRica description language."
  ;;(altaricat-mode-commun-locals)
  (setq-local font-lock-defaults '(altarica-el-highlights)))

;;;###autoload
(define-derived-mode altarica-spe-mode fundamental-mode "altarica-spe"
  "Major mode for editing AltaRica specification language."
  (altaricat-mode-commun-locals)
  (setq-local font-lock-defaults '(altarica-spe-highlights)))

(provide 'altarica-mode)
(provide 'altarica-spe-mode)
