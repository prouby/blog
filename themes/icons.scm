;;; Copyright (C) 2024 Pierre-Antoine Rouby <contact@parouby.fr>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (themes icons)
  #:use-module (ice-9 match)
  #:export (devicon-link
            icon-i
            get-icon))

;; Devicon.dev

(define devicon-link
  '(link (@ (rel "stylesheet")
            (type "text/css")
            (href "https://cdn.jsdelivr.net/gh/devicons/devicon@latest/devicon.min.css"))))

(define (icon-i name)
  `(i (@ (class ,name))))

(define (get-icon name)
  (icon-i
   (match name
     ;; System
     ("linux" "devicon-linux-plain")
     ("debian" "devicon-debian-plain")
     ("fedora" "devicon-fedora-plain")
     ;; lang
     ("c" "devicon-c-plain")
     ("cpp" "devicon-cplusplus-plain")
     ("r" "devicon-r-plain")
     ("rust" "devicon-rust-original")
     ("python" "devicon-python-plain")
     ("html" "devicon-html5-plain")
     ("bash" "devicon-bash-plain")
     ("ocaml" "devicon-ocaml-plain")
     ("haskell" "devicon-haskell-plain")
     ;; Tech
     ("apache" "devicon-apache-plain")
     ("docker" "devicon-docker-plain-wordmark")
     ("wasm" "devicon-wasm-original")
     ("qt" "devicon-qt-original")
     ("mpl" "devicon-matplotlib-plain")
     ("gcc" "devicon-gcc-plain")
     ("llvm" "devicon-llvm-plain")
     ("arduino" "devicon-arduino-plain")
     ("opencl" "devicon-opencl-plain")
     ("sdl" "devicon-sdl-plain")
     ("django" "devicon-django-plain")
     ;; DB
     ("sqlite" "devicon-sqlite-plain")
     ("mysql" "devicon-mysql-original")
     ("mariadb" "devicon-mariadb-original")
     ("postgresql" "devicon-postgresql-plain")
     ("influxdb" "devicon-influxdb-original")
     ;; Software
     ("git" "devicon-git-plain")
     ("gitlab" "devicon-gitlab-plain")
     ("ssh" "devicon-ssh-original-wordmark")
     ("cmake" "devicon-cmake-plain")
     ("vim" "devicon-vim-plain")
     ("emacs" "devicon-emacs-original")
     ("latex" "devicon-latex-original")
     ("markdown" "devicon-markdown-original")
     ("firefox" "devicon-firefox-plain")
     ("jupyter" "devicon-jupyter-plain-wordmark")
     ("gimp" "devicon-gimp-plain")
     ("inkscape" "devicon-inkscape-plain")
     ("blender" "devicon-blender-original"))))
