;;; Copyright (C) 2024 Pierre-Antoine Rouby <contact@parouby.fr>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (themes main)
  #:use-module (themes icons)
  #:use-module (commonmark)
  #:use-module (haunt asset)
  #:use-module (haunt builder blog)
  #:use-module (haunt builder atom)
  #:use-module (haunt builder assets)
  #:use-module (haunt html)
  #:use-module (haunt page)
  #:use-module (haunt post)
  #:use-module (haunt reader)
  #:use-module (haunt reader commonmark)
  #:use-module (haunt site)
  #:use-module (haunt utils)
  #:use-module (sxml match)
  #:use-module (sxml transform)
  #:use-module (sxml match)
  #:use-module (sxml transform)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-19)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:export (h1
            nav-link
            %cc-by-sa-link
            %cc-by-sa-button
            main-theme))

(define (h1 str)
  `(h1 (@ (class "title"))
       ,(string-append
         ;; "★ "
         str
         ;; " ★"
         )))

(define (nav-link label url)
  `((a (@ (class "navlink")
          (href ,url)) ,label)))

(define %cc-by-sa-link
  '(a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/")
         (target "_blank"))
      "Creative Commons Attribution Share-Alike 4.0 International"))

(define %cc-by-sa-button
  '(a (@ (class "cc-button")
         (href "https://creativecommons.org/licenses/by-sa/4.0/")
         (target "_blank"))
      (img (@ (src "https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg")
              (alt "cc-by-sa button")))))

(define main-theme
  (theme
   #:name "main"
   #:layout
   (λ (site title body)
     `((doctype "html")
       (head
        (meta (@ (charset "utf-8")))
        (title ,(string-append title " - " (site-title site)))
        ,devicon-link
        (link (@ (rel "stylesheet")
                 (href ,(string-append "/css/main.css")))))
       (body
        (header
         (div (@ (class "nav"))
              (ul (li ,(nav-link "Pierre-Antoine Rouby" "/"))
                  (li "")
                  ;; (li ,(nav-link "About" "/about.html"))
                  (li ,(nav-link "Curriculum Vitae" "/cv.html"))
                  (li ,(nav-link "Projects" "/projects.html"))
                  (li ,(nav-link "Blog" "/")))))
        (hr)
        (div (@ (class "content"))
             ,body)
        (hr)
        (footer
         (p (a (@ (rel "me")
                  (href "https://framapiaf.org/@parouby")
                  (target "_blank"))
               (img (@ (src "/images/mastodon-logo-black.svg")
                       (alter "Mastodon")
                       (width "32"))))
            (a (@ (rel "me")
                  (href "https://framagit.org/prouby")
                  (target "_blank"))
               (img (@ (src "/images/gitlab-logo-600.svg")
                       (alter "Gitlab")
                       (width "46")))))
         (p (a (@ (href "https://bff.ecoindex.fr/redirect/?url=https://parouby.fr")
                  (target "_blank"))
               (img (@ (src "https://bff.ecoindex.fr/badge/?theme=light&url=https://parouby.fr")
                       (alt "Ecoindex Badge")))))
         (p "© 2019-2024 Pierre-Antoine Rouby ("
            (a (@ (href "https://framagit.org/prouby/blog")
                  (target "_blank"))
               "source code")
            ")")
         (p "This page content are free culture works available under the "
            ,%cc-by-sa-link " license.")
         (p "This page was created with Free Software. Powered by "
            (a (@ (href "https://dthompson.us/projects/haunt.html")
                  (target "_blank"))
               "Haunt")
            " and λ with "
            (a (@ (href "https://gnu.org/s/guile")
                  (target "_blank"))
               "GNU Guile")
            ".")
         ;; (p ,%cc-by-sa-button)
         ))))
   #:post-template
   (λ (post)
     `(,(h1 (post-ref post 'title))
       (div (@ (class "date"))
            ,(date->string (post-date post)
                               "(~d ~B ~Y)"))
       (div (@ (class "post"))
            ,(post-sxml post))))
   #:collection-template
   (λ (site title posts prefix)
     (define (post-uri post)
       (string-append "/" (or prefix "")
                      (site-post-slug site post) ".html"))

     `(,(h1 title)
       ,(map (lambda (post)
               (let ((uri (string-append "/"
                                         (site-post-slug site post)
                                         ".html")))
                 `(div (@ (class "summary"))
                       (h2 (a (@ (href ,uri))
                              ,(post-ref post 'title)))
                       (div (@ (class "summary"))
                            (small (em ,(date->string (post-date post)
                                                      "(~d ~B, ~Y)  ")))
                            ,(post-ref post 'summary)))))
             posts)))))
