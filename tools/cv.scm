;;; Copyright (C) 2019 Pierre-Antoine Rouby <contact@parouby.fr>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (tools cv)
  #:use-module (themes icons)
  #:use-module (json)
  #:use-module (ice-9 match)
  #:use-module (commonmark)
  #:export (cv-json->sxml))

(define (info->sxml l)
  (define (infos l sxml)
    (match l
      (() (reverse sxml))
      (((label value) . r)
       (infos r (cons `(li (@ (class "li-info"))
                           ,label " - " ,value)
                      sxml)))))
  `((h3 "Information")
    (span
     ,(cons* 'ul '(@ (class "ul-info"))
             (infos l '())))))

(define (bio->sxml l)
  `((h3 "Bio")
    (span
     (p (@ (class "cv-bio"))
        ,(cdr (car (commonmark->sxml l)))))))

(define (contact->sxml l)
  (define (contacts l sxml)
    (match l
      (() sxml)
      (((label (url displ)) . r)
       (contacts r (cons `(li (@ (class "li-contact"))
                              (spam (@ (class "head-contact"))
                                  ,(string-append label "://"))
                              (a (@ (href ,url)
                                    (target "_blank"))
                                 ,displ))
                         sxml)))
      (((label value) . r)
       (contacts r (cons `(li (@ (class "li-contact"))
                              (em (@ (class "head-contact"))
                                  ,(string-append label "://"))
                              ,value)
                         sxml)))))
  `((h3 "Contact")
    (span
     ,(cons* 'ul '(@ (class "ul-contact"))
             (reverse (contacts l '()))))))

(define (project->sxml l)
  (define (linked name)
    `(a (@ (href ,(string-append "/projects/" name ".html"))
           (target "_blank"))
        ,name))
  (define (projects l sxml)
    (match l
      (() sxml)
      (((label value) . r)
       (projects r (cons `(li (@ (class "li-project"))
                              ,(linked label) " - "
                              ,value)
                         sxml)))))
  `((details
     (summary "Projets")
     (span
      ,(cons* 'ul '(@ (class "ul-project"))
              (reverse (projects l '())))))))

(define (association->sxml l)
  (define (associations l sxml)
    (match l
      (() sxml)
      ((((label) value) . r)
       (associations r (cons `(li (@ (class "li-association"))
                                  ,label
                                  " depuis " ,value)
                             sxml)))
      ((((label url) value) . r)
       (associations r (cons `(li (@ (class "li-association"))
                                  (strong (a (@ (href ,url)
                                                (target "_blank"))
                                             ,label))
                                  " depuis " ,value)
                         sxml)))))
  `((details
     (summary "Association")
     (span
      ,(cons* 'ul '(@ (class "ul-association"))
              (reverse (associations l '())))))))

(define (etude->sxml l)
  (define (etudes l sxml)
    (match l
      (() sxml)
      (((type name year place desc) . r)
       (etudes r (cons `(li (@ (class "li-etude"))
                                  (strong ,type) " - " ,name (br)
                                  " | " ,year " | " ,place (br)
                                  " | " ,desc)
                         sxml)))))
  `((details
     (summary "Étude en cours")
     (span
      ,(cons* 'ul '(@ (class "ul-etude"))
              (reverse (etudes l '())))))))

(define (diplome->sxml l)
  (define (diplomes l sxml)
    (match l
      (() sxml)
      (((type lvl name year place mention) . r)
       (diplomes r (cons `(li (@ (class "li-diplome"))
                                  (strong ,type) " (" ,lvl ") ",name (br)
                                  (em ,year) " | " ,place
                                  ,@(if (string=? mention "")
                                        '("")
                                        `(" | " (em ,mention))))
                         sxml)))))
  `((details
     (summary "Diplômes")
     (span
      ,(cons* 'ul '(@ (class "ul-diplome"))
              (reverse (diplomes l '())))))))

(define (exp->sxml l)
  (define (work->sxml str)
    `(li (@ (class "li-exp-work"))
         ,str))
  (define (exps l sxml)
    (match l
      (() sxml)
      (((inc type years place works techs) . r)
       (exps r (cons `(li (@ (class "li-exp"))
                          (strong ,inc) " | " (em ,type) (br)
                          (em ,years) " | " ,place (br)
                          ,(cons* 'ul '(@ (class "ul-exp-work"))
                                  (map work->sxml works))
                          ,(map get-icon techs))
                     sxml)))))
  `((details
     (summary "Expériences")
     (span
      ,(cons* 'ul '(@ (class "ul-exp"))
              (reverse (exps l '())))))))

(define (hobbies->sxml l)
  (define (hobbies-map s)
    `(li (@ (class "li-hobbies"))
         ,s))
  `((details
     (summary "Loisirs")
     (span
      ,(cons* 'ul '(@ (class "ul-hobbies"))
              (map hobbies-map l))))))

(define (competence->sxml l)
  (define (make-color-star n)
    (if (> n 0)
        (string-append "★" (make-color-star (- n 1)))
        ""))
  (define (make-li l)
    (match l
      ((x n)
       `(li (@ (class "li-with-star"))
            (span (@ (class "color-star"))
                  ,(make-color-star n))
            (span (@ (class "black-star"))
                  ,(make-color-star (- 5 n)))
            " " ,x))))
  (define (competences l sxml)
    (match l
      (() sxml)
      (((type l) . r)
       (competences r (cons `((h3 ,type)
                              ,(map make-li l))
                            sxml)))))
  `((span
     ,(reverse (competences l '())))))

(define (skills->sxml l li)
  (define (skills sk acc)
    (match sk
      (() (reverse acc))
      (((type lst) . r)
       (skills r
               (cons
                `(li (strong ,(format #f "~a: " type))
                     ,(string-join lst " · "))
                acc)))))
  `((details
     (summary "Connaissances")
     (span
      (ul (@ (class "ul-skills"))
          ,(skills l '()))
      (div (@ (class "skills-icons"))
           ,(map (lambda (lli)
                   (cons* (map get-icon lli)
                          '((br))))
                 li))))))

(define (cv-alist->sxml alist)
  `((div (@ (class "cv"))
         (table
          (tr
           (td
            (div (@ (class "cv-c1"))
                 ,(bio->sxml         (json-ref alist "bio"))
                 (table
                  (tr
                   (td ,(info->sxml        (json-ref alist "info")))
                   (td ,(contact->sxml     (json-ref alist "contact")))))
                 ,(exp->sxml         (json-ref alist "exp"))
                 ,(diplome->sxml     (json-ref alist "diplome"))
                 ,(skills->sxml      (json-ref alist "skills")
                                     (json-ref alist "skills-icons")))
            (div (@ (class "cv-c2"))
                 ;; ,(etude->sxml       (json-ref alist "etude"))
                 ,(association->sxml (json-ref alist "association"))
                 ;; ,(project->sxml     (json-ref alist "project"))
                 ,(hobbies->sxml     (json-ref alist "loisir"))))
           (td
            (div (@ (class "cv-c3"))
                 ,(competence->sxml  (json-ref alist "competence")))))))))

(define (json-ref data ref)
  (define (vector-list->list vl)
    (cond
     ((vector? vl) (map vector-list->list (vector->list vl)))
     ((list? vl)   vl)
     (else         vl)))
  (define (get-ref conf key)
    (cond
     ((hash-table? conf)  (hash-ref conf key))
     (else                (assoc-ref conf key))))
  (vector-list->list (get-ref data ref)))

(define (cv-json->sxml file)
  "Make sxml cv from json FILE."
  (let* ((fd   (open-file file "r"))
         (sxml (cv-alist->sxml (json->scm fd))))
    (close-port fd)
    sxml))

;;; cv.scm ends here.
