;;; Copyright (C) 2024 Pierre-Antoine Rouby <contact@parouby.fr>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;; Some of highlight function come from David Thompson blog.
;;   https://git.dthompson.us/blog.git

(define-module (tools highlight)
  #:use-module (commonmark)
  #:use-module (haunt page)
  #:use-module (haunt post)
  #:use-module (haunt reader)
  #:use-module (haunt reader commonmark)
  #:use-module (syntax-highlight)
  #:use-module (syntax-highlight scheme)
  #:use-module (syntax-highlight xml)
  #:use-module (syntax-highlight c)
  #:use-module (sxml match)
  #:use-module (sxml transform)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:export (maybe-highlight-code
            sxml-identity
            highlight-code
            highlight-scheme
            md-video
            %commonmark-rules
            post-process-commonmark
            commonmark-reader*))

(define (maybe-highlight-code lang source)
  (let ((lexer (match lang
                 ('scheme lex-scheme)
                 ('xml    lex-xml)
                 ('c      lex-c)
                 (_ #f))))
    (if lexer
        (highlights->sxml (highlight lexer source))
        source)))

(define (sxml-identity . args) args)

(define (highlight-code . tree)
  (sxml-match tree
    ((code (@ (class ,class) . ,attrs) ,source)
     (let ((lang (string->symbol
                  (string-drop class (string-length "language-")))))
       `(code (@ ,@attrs)
             ,(maybe-highlight-code lang source))))
    (,other other)))

(define (highlight-scheme code)
  `(pre (code ,(highlights->sxml (highlight lex-scheme code)))))

(define (md-video . tree)
  (sxml-match tree
    ((img (@ (src ,src) . ,attrs) . ,body)
     (cond
      ((string-suffix? ".webm" src)
       `(video (@ (src ,src) (controls "true"),@attrs) ,@body))
      ((string-match "peertube@" src)
       (let ((url (list-ref (string-split src #\@) 1)))
         `(iframe (@ (width "560")
                     (height "315")
                     (sandbox "allow-same-origin allow-scripts")
                     (src ,url)
                     ,@attrs)
                  ,@body)))
      (else tree)))))

(define %commonmark-rules
  `((code . ,highlight-code)
    (img  . ,md-video)
    (*text* . ,(lambda (tag str) str))
    (*default* . ,sxml-identity)))

(define (post-process-commonmark sxml)
  (pre-post-order sxml %commonmark-rules))

(define commonmark-reader*
  (make-reader (make-file-extension-matcher "md")
               (lambda (file)
                 (call-with-input-file file
                   (lambda (port)
                     (values (read-metadata-headers port)
                             (post-process-commonmark
                              (commonmark->sxml port))))))))
